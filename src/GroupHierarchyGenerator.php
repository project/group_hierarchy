<?php

namespace Drupal\group_hierarchy;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\NullBackend;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TempStore\SharedTempStore;
use Drupal\Core\TempStore\SharedTempStoreFactory;
use Drupal\group\Entity\GroupInterface;

/**
 * The group hierarchy generator service.
 */
class GroupHierarchyGenerator implements GroupHierarchyGeneratorInterface {

  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The logger channel service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected LoggerChannelFactoryInterface $logger;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * The temp store service.
   *
   * @var \Drupal\Core\TempStore\SharedTempStore
   */
  protected SharedTempStore|SharedTempStoreFactory $tempStore;

  /**
   * Constructs a GroupHierarchyGenerator object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   Logger factory object.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param \Drupal\Core\TempStore\SharedTempStoreFactory $temp_store
   *   The factory for the temp store object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LoggerChannelFactoryInterface $logger, MessengerInterface $messenger, CacheBackendInterface $cache, SharedTempStoreFactory $temp_store) {
    $this->cache = $cache;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->messenger = $messenger;
    $this->tempStore = $temp_store;
  }

  /**
   * {@inheritdoc}
   */
  public function setBatch():  void {
    $batch_builder = (new BatchBuilder())
      ->setTitle($this->t('Generating group hierarchy'))
      ->setFinishCallback([$this, 'finishBatch'])
      ->setInitMessage($this->t('Starting group hierarchy.'))
      ->setProgressMessage($this->t('Completed @current of @total.'))
      ->setErrorMessage($this->t('Group hierarchy has encountered an error.'));

    // Add an operation foreach root group type.
    $storage = $this->entityTypeManager->getStorage('group_type');
    $root_group_type_ids = $storage
      ->getQuery()
      ->condition('third_party_settings.subgroup.' . SUBGROUP_DEPTH_SETTING, 0)
      ->accessCheck(FALSE)
      ->execute();

    foreach ($root_group_type_ids as $group_type_id) {
      $batch_builder->addOperation([$this, 'processBatch'], [$group_type_id]);
    }

    batch_set($batch_builder->toArray());
  }

  /**
   * {@inheritdoc}
   */
  public function processBatch(string $group_type_id, array &$context):  void {
    $storage = $this->entityTypeManager->getStorage('group');

    if (empty($context['sandbox']['subgroups']) && (empty($context['sandbox']['group_type_id']) || $context['sandbox']['group_type_id'] != $group_type_id)) {
      $context['sandbox']['batch_size'] = $context['sandbox']['batch_size'] ?? Settings::get('entity_update_batch_size', 50);
      // Initialize the group hierarchy if it is empty.
      $context['results']['groups_by_parent'] = $context['results']['groups_by_parent'] ?? [];

      // Get the root groups for the specific bundle.
      $context['sandbox']['group_type_id'] = $group_type_id;
      $context['sandbox']['groups'] = $storage
        ->getQuery()
        ->condition('type', $group_type_id)
        ->sort('label')
        ->accessCheck(FALSE)
        ->execute();
    }

    // Get all the root groups' descendants and load them.
    if (!empty($context['sandbox']['groups'])) {
      $group_id = array_shift($context['sandbox']['groups']);
      /** @var \Drupal\group\Entity\GroupInterface $group */
      if ($group = $storage->load($group_id)) {
        $context['results']['groups_by_parent'][0][] = $group;

        $descendant = $this->getDescendantIds($group);
        if (!empty($descendant)) {
          $context['sandbox']['subgroups'][$group_id] = $descendant;
        }
      }
    }
    elseif (!empty($context['sandbox']['subgroups'])) {
      $ancestor_id = array_key_first($context['sandbox']['subgroups']);
      $group_ids = array_splice($context['sandbox']['subgroups'][$ancestor_id], 0, $context['sandbox']['batch_size'], []);

      /** @var \Drupal\group\Entity\GroupInterface $descendant */
      foreach ($storage->loadMultiple($group_ids) as $descendant) {
        $parent_id = $this->getParentId($descendant);
        $context['results']['groups_by_parent'][$parent_id][] = $descendant;
      }

      if (empty($context['sandbox']['subgroups'][$ancestor_id])) {
        unset($context['sandbox']['subgroups'][$ancestor_id]);
      }
    }

    $context['finished'] = (int) (empty($context['sandbox']['groups']) && empty($context['sandbox']['subgroups']));
  }

  /**
   * {@inheritdoc}
   */
  public function finishBatch(bool $success, array $results):  void {
    if ($success) {
      // Save the group hierarchy into cache or in the temp store
      // if the cache is disabled.
      if ($this->cache instanceof NullBackend) {
        $this->tempStore->get('group_hierarchy_temp_store')
          ->set('group_hierarchy', $results['groups_by_parent']);
      }
      else {
        $this->cache->set('group_hierarchy', $results['groups_by_parent']);
      }

      $this->messenger->addStatus($this->t('The group hierarchy have be updated.'));
      $this->logger()->notice('The group hierarchy have be updated.');
    }
    else {
      $this->messenger->addStatus($this->t('The group hierarchy could not be updated.'));
      $this->logger()->warning('The group hierarchy could not be updated.');
    }
  }

  /**
   * Gets the descendants of a group.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The entity to retrieve the descendants for.
   *
   * @return array
   *   The entity IDs that represent the leaf's descendants in the tree.
   */
  protected function getDescendantIds(GroupInterface $group):  array {
    $leaf = $this->wrapLeaf($group);

    return $this->entityTypeManager
      ->getStorage('group')
      ->getQuery()
      ->condition(SUBGROUP_LEFT_FIELD, $leaf->getLeft(), '>')
      ->condition(SUBGROUP_RIGHT_FIELD, $leaf->getRight(), '<')
      ->condition(SUBGROUP_TREE_FIELD, $leaf->getTree())
      ->sort(SUBGROUP_LEFT_FIELD)
      ->accessCheck(FALSE)
      ->execute();
  }

  /**
   * Gets the parent ID of a group.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The entity to retrieve the parent for.
   *
   * @return string|false
   *   The entity ID that represents the leaf's parent in the tree.
   */
  protected function getParentId(GroupInterface $group):  string|false {
    $leaf = $this->wrapLeaf($group);

    if ($this->wrapLeaf($group)->getDepth() === 0) {
      return FALSE;
    }

    $entity_ids = $this->entityTypeManager
      ->getStorage('group')
      ->getQuery()
      ->condition(SUBGROUP_DEPTH_FIELD, $leaf->getDepth() - 1)
      ->condition(SUBGROUP_LEFT_FIELD, $leaf->getLeft(), '<')
      ->condition(SUBGROUP_RIGHT_FIELD, $leaf->getRight(), '>')
      ->condition(SUBGROUP_TREE_FIELD, $leaf->getTree())
      ->accessCheck(FALSE)
      ->execute();

    return reset($entity_ids);
  }

  /**
   * Wraps a group in a LeafInterface.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group to wrap.
   *
   * @return \Drupal\subgroup\LeafInterface
   *   The wrapped group.
   */
  protected function wrapLeaf(GroupInterface $group) {
    $class = $this->entityTypeManager
      ->getDefinition($group->getEntityTypeId())
      ->get('subgroup_wrapper');

    return new $class($group);
  }

  /**
   * Gets the logger for a specific channel.
   *
   * @param string $channel
   *   The name of the channel. Can be any string, but the general practice is
   *   to use the name of the subsystem calling this.
   *
   * @return \Psr\Log\LoggerInterface
   *   The logger for the given channel.
   */
  protected function logger(string $channel = 'group_hierarchy') {
    return $this->logger->get($channel);
  }

}
