<?php

namespace Drupal\group_hierarchy\Controller;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Cache\NullBackend;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityListBuilderInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\TempStore\SharedTempStore;
use Drupal\Core\TempStore\SharedTempStoreFactory;
use Drupal\Core\Url;
use Drupal\group_hierarchy\GroupHierarchyGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Returns responses for group hierarchy routes.
 */
class GroupHierarchyController extends ControllerBase {

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cache;

  /**
   * The group list builder.
   *
   * @var \Drupal\Core\Entity\EntityListBuilderInterface
   */
  protected EntityListBuilderInterface $groupListBuilder;

  /**
   * The group hierarchy generator service.
   *
   * @var \Drupal\group_hierarchy\GroupHierarchyGeneratorInterface
   */
  protected GroupHierarchyGeneratorInterface $hierarchyGenerator;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * The temp store service.
   *
   * @var \Drupal\Core\TempStore\SharedTempStore
   */
  protected SharedTempStore|SharedTempStoreFactory $tempStore;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The cache backend.
   * @param \Drupal\Core\TempStore\SharedTempStoreFactory $temp_store
   *   The factory for the temp store object.
   * @param \Drupal\group_hierarchy\GroupHierarchyGeneratorInterface $hierarchy_generator
   *   The group hierarchy generator service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(CacheBackendInterface $cache, SharedTempStoreFactory $temp_store, GroupHierarchyGeneratorInterface $hierarchy_generator, EntityTypeManagerInterface $entity_type_manager, RendererInterface $renderer) {
    $this->cache = $cache;
    $this->groupListBuilder = $entity_type_manager->getListBuilder('group');
    $this->hierarchyGenerator = $hierarchy_generator;
    $this->renderer = $renderer;
    $this->tempStore = $temp_store;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('cache.default'),
      $container->get('tempstore.shared'),
      $container->get('group_hierarchy.generator'),
      $container->get('entity_type.manager'),
      $container->get('renderer')
    );
  }

  /**
   * Displays the group hierarchy.
   */
  public function build(): array|RedirectResponse|null {
    // Get the group hierarchy from the cache or from the temp store
    // if the cache is disabled.
    if ($this->cache instanceof NullBackend) {
      $group_hierarchy = $this->tempStore->get('group_hierarchy_temp_store')
        ->get('group_hierarchy');
    }
    else {
      $cached = $this->cache->get('group_hierarchy');
      $group_hierarchy = $cached->data ?? NULL;
    }

    // The group hierarchy calculation is a very heavy process depending
    // on the number of groups there are, so we calculate it via batch,
    // if it doesn't exist, we force its creation.
    if (empty($cached) && $group_hierarchy === NULL) {
      $this->hierarchyGenerator->setBatch();
      if ($response = batch_process(Url::fromRoute('group_hierarchy')->toString())) {
        return $response;
      }
    }

    $rows = [];
    $groups = $this->buildTree($group_hierarchy[0] ?? [], $group_hierarchy);
    foreach ($groups as $group) {
      $depth = (int) $group->get(SUBGROUP_DEPTH_FIELD)->value;
      $indentation = [];
      if ($depth > 0) {
        $indentation = [
          '#theme' => 'indentation',
          '#size' => $depth,
        ];
      }
      if ($group_operations = $this->groupListBuilder->getOperations($group)) {
        $operations = [
          '#type' => 'operations',
          '#links' => $group_operations,
        ];
      }
      else {
        $operations = [];
      }

      $rows[] = [
        'group' => [
          '#prefix' => !empty($indentation) ? $this->renderer->render($indentation) : '',
          '#type' => 'link',
          '#title' => $group->label(),
          '#url' => $group->toUrl(),
        ],
        'bundle' => [
          '#markup' => $group->type->entity->label(),
        ],
        'operations' => $operations,
      ];
    }

    $build['group_hierarchy'] = [
      '#type' => 'table',
      '#empty' => $this->t(
        'No groups available. <a href=":link">Add one</a>.',
        [':link' => Url::fromRoute('entity.group.collection')->toString()]),
      '#header' => [
        $this->t('Name'),
        $this->t('Bundle'),
        $this->t('Operations'),
      ],
      '#attributes' => [
        'id' => 'group-hierarchy',
      ],
      '#cache' => [
        'tags' => [
          'group_hierarchy',
        ],
        'context' => [
          'languages:language_interface',
          'theme',
        ],
      ],
    ] + $rows;

    return $build;
  }

  /**
   * Builds the group's tree.
   *
   * @param array $roots
   *   The parent groups.
   * @param array $group_hierarchy
   *   All group hierarchy.
   *
   * @return \Drupal\group\Entity\GroupInterface[]
   *   The group's tree.
   */
  protected function buildTree(array $roots, array $group_hierarchy): array {
    $tree = [];
    /** @var \Drupal\group\Entity\GroupInterface $group */
    foreach ($roots as $group) {
      $tree[] = $group;
      // If the current root has parents, get them.
      if (!empty($group_hierarchy[$group->id()])) {
        $tree = array_merge(
          $tree,
          $this->buildTree($group_hierarchy[$group->id()], $group_hierarchy)
        );
      }
    }

    return $tree;
  }

}
