<?php
// phpcs:ignoreFile

/**
 * This file was generated via php core/scripts/generate-proxy-class.php 'Drupal\group_hierarchy\GroupHierarchyGenerator' "modules/group_hierarchy/src".
 */
namespace Drupal\group_hierarchy\ProxyClass {

    /**
     * Provides a proxy class for \Drupal\group_hierarchy\GroupHierarchyGenerator.
     *
     * @see \Drupal\Component\ProxyBuilder
     */
    class GroupHierarchyGenerator implements \Drupal\group_hierarchy\GroupHierarchyGeneratorInterface
    {

        use \Drupal\Core\DependencyInjection\DependencySerializationTrait;

        /**
         * The id of the original proxied service.
         *
         * @var string
         */
        protected $drupalProxyOriginalServiceId;

        /**
         * The real proxied service, after it was lazy loaded.
         *
         * @var \Drupal\group_hierarchy\GroupHierarchyGenerator
         */
        protected $service;

        /**
         * The service container.
         *
         * @var \Symfony\Component\DependencyInjection\ContainerInterface
         */
        protected $container;

        /**
         * Constructs a ProxyClass Drupal proxy object.
         *
         * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
         *   The container.
         * @param string $drupal_proxy_original_service_id
         *   The service ID of the original service.
         */
        public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container, $drupal_proxy_original_service_id)
        {
            $this->container = $container;
            $this->drupalProxyOriginalServiceId = $drupal_proxy_original_service_id;
        }

        /**
         * Lazy loads the real service from the container.
         *
         * @return object
         *   Returns the constructed real service.
         */
        protected function lazyLoadItself()
        {
            if (!isset($this->service)) {
                $this->service = $this->container->get($this->drupalProxyOriginalServiceId);
            }

            return $this->service;
        }

        /**
         * {@inheritdoc}
         */
        public function setBatch(): void
        {
            $this->lazyLoadItself()->setBatch();
        }

        /**
         * {@inheritdoc}
         */
        public function processBatch(string $group_type_id, array &$context): void
        {
            $this->lazyLoadItself()->processBatch($group_type_id, $context);
        }

        /**
         * {@inheritdoc}
         */
        public function finishBatch(bool $success, array $results): void
        {
            $this->lazyLoadItself()->finishBatch($success, $results);
        }

        /**
         * {@inheritdoc}
         */
        public function __sleep()
        {
            return $this->lazyLoadItself()->__sleep();
        }

        /**
         * {@inheritdoc}
         */
        public function __wakeup()
        {
            return $this->lazyLoadItself()->__wakeup();
        }

        /**
         * {@inheritdoc}
         */
        public function setStringTranslation(\Drupal\Core\StringTranslation\TranslationInterface $translation)
        {
            return $this->lazyLoadItself()->setStringTranslation($translation);
        }

    }

}
