<?php

namespace Drupal\group_hierarchy;

/**
 * Defines the interface for the group hierarchy generator service.
 */
interface GroupHierarchyGeneratorInterface {

  /**
   * Creates a group hierarchy batch to refresh the group hierarchy.
   */
  public function setBatch():  void;

  /**
   * Implements callback_batch_operation().
   *
   * Updates the group hierarchy.
   *
   * @param string $group_type_id
   *   The root group type ID.
   * @param array|\ArrayAccess $context
   *   Contains a list of group.
   */
  public function processBatch(string $group_type_id, array &$context):  void;

  /**
   * Implements callback_batch_finished().
   *
   * Finishes callback of group hierarchy.
   *
   * @param bool $success
   *   Information about the success of the batch.
   * @param array $results
   *   Information about the results of the batch.
   */
  public function finishBatch(bool $success, array $results):  void;

}
