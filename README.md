Group Hierarchy
===============

This module allows users to view the groups and their hierarchy.

### SPONSORS
- Initial development: [Amara NZero](https://amaranzero.com)

### CONTACT
Developed and maintained by Cambrico (http://cambrico.net).

Get in touch with us for customizations and consultancy:
http://cambrico.net/contact

#### Current maintainers:
- Pedro Cambra [(pcambra)](http://drupal.org/user/122101)
- Manuel Egío [(facine)](http://drupal.org/user/1169056)
